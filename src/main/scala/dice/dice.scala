/**
 * Core dice rolling functions and types.
 * @author Sean McCauliff
 */

package dice
import scala.collection.mutable.{Map => MutableMap}

/**
 * A die is just some sequence of faces of some type for example the numbers one
 * through six.
 */
trait Die[F] {
    def faces : Seq[F]
}


/**
 * UnfairDie is a die that does not have a uniform probability distribution.
 * In some cases die rolls are openended that is we get a certain number than
 * we want to roll them again. This can be useful for simulating openended rolls
 * rolling a 6 and the rolling again can be a new "face" on this virutal die that
 * has probability 1/6 * 1/6.  Using rational numbers of probability
 */
case class UnFairFace[F](val face : F, val probability : Double)

trait UnfarDie[F] {
    def faces : Seq[UnFairFace[F]]
}

/**
 * @tparam F The dice face type
 * @tparam D The die type
 * @tparam R The result type from combining rolls.
 */
object Dice {

    private def permute[F, D <: Die[F], R](hand : List[D],
                rolled : List[F],
                rollCombiner : Seq[F] => R,
                results : MutableMap[R , Int]) : Unit  = {
        if (hand.isEmpty) {
            val combinedRoll = rollCombiner(rolled)
            results += (combinedRoll -> (results.getOrElse(combinedRoll, 0) + 1))
        } else {
            val die = hand.head
            die.faces.foreach( (face) => {
                permute(hand.tail, face :: rolled, rollCombiner, results)
            })
        }
    }
    
    /**
     * @param hand These are the dice that are going to be rolled.
     * @param rollCombiner This function takes a sequence of faces that were rolled
     * and produces some result of result type R.  For example a rollCombiner for
     * rolling two six sided dice would add both faces together.
     */
    def generateProbabilities[F, D <: Die[F], R](hand : List[D], rollCombiner : Seq[F] => R) : MutableMap[R, Double] = {
        var distinctPermutations = scala.collection.mutable.HashMap.empty[R, Int]
        //TODO:  Need to change this if for some reason we don't have a uniform
        //probability distribution, (e.g. if we want to simulate open ended die
        //rolls).
        val nPermutations = hand.map(_.faces.length).foldLeft(1)( (a, b) => a * b).toDouble
        permute(hand, List.empty[F], rollCombiner, distinctPermutations)
        distinctPermutations.map((p) => (p._1 -> p._2.toDouble / nPermutations))
    }
    
}
