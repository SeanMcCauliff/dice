/**
* The game Star Wars : Edge of the Empire uses kind of strange dice with
* sometimes multiple symbols of different types on the same face.
*
* See http://www.fantasyflightgames.com/edge_news.asp?eidn=3746
* 
* @author Sean McCauliff
*/

package dice


import scala.math.Ordered


/**
 * The faces that exist among all the dice.
 * This could be an Enumeration, but the scala Enumeration class is kind of
 * strange.
*/
sealed abstract class SwFace(val order : Int) extends Ordered[SwFace] {
    override def compare(that : SwFace) = this.order - that.order
}

object SwFace {
    // This could be represented as an empty list instead of explicitally mentioning
    // this as a blank. But having it here means you can calculate the probability of
    // rolling it if you want.
    case object Blank        extends SwFace(0)
    case object Success      extends SwFace(1)
    case object Advantage    extends SwFace(2)
    case object Triumph      extends SwFace(3)
    case object Failure      extends SwFace(4)
    case object Threat       extends SwFace(5)
    case object Despair      extends SwFace(6)

    val values : List[SwFace] = List(Blank, Success, Advantage, Triumph, Failure, Threat, Despair)
    
    val BlankL     = List(Blank)
    val SuccessL   = List(Success)
    val AdvantageL = List(Advantage)
    val TriumphL   = List(Triumph)
    val FailureL   = List(Failure)
    val ThreatL    = List(Threat)
    val DespairL   = List(Despair)
}

trait SwDie extends Die[List[SwFace]] {
    def color : String
}

case object BoostDie extends SwDie {
    import SwFace._
    def faces = List(BlankL, BlankL, SuccessL, 
                     List(Success,Advantage), List(Advantage,  Advantage), AdvantageL) 
    override def color = "light blue"
}

case object SetbackDie extends SwDie {
    import SwFace._
    override def faces = List(BlankL, BlankL, FailureL, 
                              FailureL, ThreatL, ThreatL)
    override def color = "black"
}

case object AbilityDie extends SwDie {
    import SwFace._
    override def faces = List(BlankL, SuccessL, SuccessL,
                              List(Success, Success), AdvantageL, AdvantageL,
                              List(Success, Advantage), List(Advantage, Advantage))
   override def color = "green"
}

case object DifficultyDie extends SwDie {
    import SwFace._
    override def faces = List(BlankL, FailureL, List(Failure, Failure),
                            ThreatL, ThreatL, ThreatL,
                            List(Threat, Threat), List(Failure, Threat))
    override def color = "purple"
}


case object ProficencyDie extends SwDie {
    import SwFace._
    override def faces = 
        List(BlankL, SuccessL, SuccessL,
             List(Success, Success),List(Success, Success), AdvantageL,
             List(Success, Advantage), List(Success, Advantage), List(Success, Advantage),
             List(Advantage, Advantage),List(Advantage, Advantage), TriumphL)
    override def color = "yellow"
}

case object ChallengeDie extends SwDie {
    import SwFace._
    override def faces = List(BlankL, FailureL, FailureL,
              List(Failure, Failure), List(Failure, Failure), ThreatL,
              ThreatL, List(Failure, Threat), List(Failure, Threat),
              List(Threat, Threat), List(Threat, Threat), DespairL)
    override def color = "red"
}

/**
 *  The counts of this class can be negative in which case they represent more of the
 * opposing faces where rolled.  Threat vs Advantage, Success vs Failure, Triumph vs Despair
 */
case class RollResult(var successCount : Int, var advantageCount : Int,
                     var triumphCount : Int)
                     
/*  There are a few other dice used in Star Wars Edge of the Empire, but these don't
 * interact with how challenges are resolved.
 */

object Sweote extends App {
    import dice.Dice._
    
    def combineRoll(roll : Seq[List[SwFace]]) : RollResult = {
        var successCount = 0;
        var advantageCount = 0;
        var triumphCount = 0;
        
        import dice.SwFace._
        
        roll.foreach( (l) => l.foreach((f) => f match {
            case Success   => successCount   += 1
            case Failure   => successCount   -= 1
            case Advantage => advantageCount += 1
            case Threat    => advantageCount -= 1
            case Triumph   => triumphCount   += 1
            case Despair   => triumphCount   -= 1
            case Blank     => 
            })
        )
        
        RollResult(successCount, advantageCount, triumphCount)
        
    }
    
    val probabilities : List[(RollResult, Double)] = 
        generateProbabilities(
        List(ProficencyDie, AbilityDie, DifficultyDie, DifficultyDie, BoostDie),
        combineRoll)
        .toList.sortBy( _._2)
        
    probabilities.foreach{ (x) => x match {
        case (RollResult(success, advantage, triumph), prob)  => {
            println( f"$success%d,$advantage%d,$triumph%d, $prob%.5f")
        }}}
        
    println( probabilities.map( x => x match {
        case (RollResult(success,advantage,_), prob) => if (Math.abs(advantage) > 2) { prob } else { 0 }
    }).foldLeft(0.0)( _+_ ) )
    
}
