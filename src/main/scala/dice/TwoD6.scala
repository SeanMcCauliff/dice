package dice

case object SixSidedDie extends Die[Int] {
    def faces = 1 to 6
}


object TwoD6 extends App {
    import Dice._
    
    generateProbabilities(
        List(SixSidedDie, SixSidedDie),
        (x : Seq[Int]) => x(0) + x(1))
    .foreach( println _ )

}
